import java.io.*;
import java.util.Scanner;

/**
 * Created by RENT on 2017-08-17.
 */
public class FileReadStrategy implements IInputStrategy {
    private Scanner scanner;
    private File file;

    public FileReadStrategy() {
        this.file = new File("input.txt");
        try {
            this.scanner = new Scanner(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getInt() {
        return scanner.nextInt();
    }

    @Override
    public String getString() {
        String string = scanner.next();
       return string;
    }

    @Override
    public double getDouble() {

        return scanner.nextDouble();

    }
    
}
