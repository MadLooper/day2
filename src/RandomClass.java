import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * Created by RENT on 2017-08-17.
 */
public class RandomClass implements IInputStrategy {

    @Override
    public int getInt() {
        return new Random().nextInt();
    }

    @Override
    public String getString() {
        Random random =new Random();
        Long.toString (Math.abs (random.nextLong ()), 36);
        return String.valueOf(random);
        }

    @Override
    public double getDouble() {
        return new Random().nextDouble();
    }
}
