import java.io.FileNotFoundException;

public interface IInputStrategy {
    int getInt() throws FileNotFoundException;
    String getString();
    double getDouble();
}
