import java.util.Scanner;

/**
 * Created by RENT on 2017-08-17.
 */
public class ScannerClass implements IInputStrategy {

Scanner scanner = new Scanner(System.in);
    @Override
    public int getInt() {
        return scanner.nextInt();
    }

    @Override
    public String getString() {
        return scanner.nextLine();
    }

    @Override
    public double getDouble() {
        return scanner.nextDouble();
    }
}
